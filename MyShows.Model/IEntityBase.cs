﻿namespace MyShows.Model
{
    public interface IEntityBase
    {
        string Id { get; set; }
    }
}
