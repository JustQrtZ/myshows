﻿using System;

namespace MyShows.Model.Entities
{
    public class Comment
    {
        public string PostId { get; set; }
        public Post Post { get; set; }
        
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }
        
        public User Owner { get; set; }
        public string OwnerId { get; set; }
    }
}