﻿using System;
using System.Collections.Generic;

namespace MyShows.Model.Entities
{
    public class Post : IEntityBase
    {
        public Post() {}
        public string Id { get; set; }
        public string Title { get; set; }
        public string TitleEng { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime TotalDuration { get; set; }
        public float RatingMyShows { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int ViewersCountTotal { get; set; }
        public int ViewersCountMounth { get; set; }
        #nullable enable
        public float? RatingImdb { get; set; }
        public float? RatingKinopoisk { get; set; }
        public string? Trailer { get; set; }
        public DateTime? EpisodeDuration { get; set; }
        #nullable disable
    }
}