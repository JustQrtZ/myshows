﻿namespace MyShows.Model.Entities
{
    public class Tag : IEntityBase
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }
}