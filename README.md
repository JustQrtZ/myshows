# MyShows back

![all text](https://cdn.auth0.com/blog/webapps-aspnet2-react/logo.png)

## Requirements:
 - .NET Core 5+
 - SQL Server(connection string specified in MyShows.API/appsettings.json)

## Run locally
```bash
git clone https://gitlab.com/JustQrtZ/myshows.git
cd MyShows.API
dotnet tool install --global dotnet-ef (if not set)
dotnet ef database update
dotnet run 
```