﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyShows.API.Services.Abstraction;
using MyShows.Data.Abstract;
using MyShows.Model.Entities;

namespace MyShows.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        IUserRepository userRepository;

        public PostsController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        
        [HttpGet("all")]
        public ActionResult<List<User>> Get()
        {
            var allusers = userRepository.GetAll();
            return allusers.ToList();
        }
    }
}