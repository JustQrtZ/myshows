﻿namespace MyShows.API.ViewModels
{
    public class PostViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string TitleEng { get; set; }
        public string Image { get; set; }
      
    }
}