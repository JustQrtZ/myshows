﻿using System.Collections.Generic;

namespace MyShows.API.ViewModels
{
    public class PostsViewModel
    {
        public List<PostViewModel> Stories { get; set; }
    }
}