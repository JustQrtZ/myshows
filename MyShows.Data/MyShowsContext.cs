using System.Collections.Generic;
using System.Linq;
using MyShows.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MyShows.Data
{
    public class MyShowsContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        
        public MyShowsContext(DbContextOptions<MyShowsContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            ConfigureModelBuilderForUser(modelBuilder);
            ConfigureModelBuilderForPost(modelBuilder);
            ConfigureModelBuilderForComment(modelBuilder);
            ConfigureModelBuilderForTag(modelBuilder);
            ConfigureModelBuilderForPostTags(modelBuilder);


        }

        void ConfigureModelBuilderForUser(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<User>()
                .Property(user => user.Username)
                .HasMaxLength(60)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(user => user.Email)
                .HasMaxLength(60)
                .IsRequired();
        }

        void ConfigureModelBuilderForPost(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>().ToTable("Post");
            modelBuilder.Entity<Post>()
                .Property(post => post.Title)
                .HasMaxLength(150);
            
            modelBuilder.Entity<Post>()
                .Property(post => post.TitleEng)
                .HasMaxLength(150);
        }
        
        void ConfigureModelBuilderForComment(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>().ToTable("Comment");
            modelBuilder.Entity<Comment>().HasKey(l => new { l.OwnerId, l.PostId });
        }
        
        void ConfigureModelBuilderForTag(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tag>().ToTable("Tag");
        }
        void ConfigureModelBuilderForPostTags(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PostTags>().ToTable("PostTags");
            modelBuilder.Entity<PostTags>().HasKey(l => new { l.PostId, l.TagId });
        }
        
        
    }
}