﻿using MyShows.Model.Entities;

namespace MyShows.Data.Abstract
{
    public interface IPostTagsRepository : IEntityBaseRepository<PostTags>
    {
        
    }
    
    
}