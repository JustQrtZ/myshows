﻿using MyShows.Model.Entities;

namespace MyShows.Data.Abstract
{
    public interface IPostRepository : IEntityBaseRepository<Post>
    {
        //TODO: Реализовать админку
        //bool IsAdmin(string postId, string userId);   
    }
}