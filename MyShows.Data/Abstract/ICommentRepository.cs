﻿using System.Collections.Generic;
using MyShows.Model.Entities;

namespace MyShows.Data.Abstract
{
    public interface ICommentRepository : IEntityBaseRepository<Comment>
    {
     
    }
}