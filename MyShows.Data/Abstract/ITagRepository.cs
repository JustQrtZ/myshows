﻿using MyShows.Model.Entities;

namespace MyShows.Data.Abstract
{
    public interface ITagRepository : IEntityBaseRepository<Tag>
    {
        
    }
}