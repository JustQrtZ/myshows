﻿using MyShows.Data.Abstract;
using MyShows.Model.Entities;

namespace MyShows.Data.Repositories
{
    public class TagRepository : EntityBaseRepository<Tag>, ITagRepository
    {
        public TagRepository(MyShowsContext context) : base(context) { }
    }
}