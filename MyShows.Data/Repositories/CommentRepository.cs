﻿using MyShows.Data.Abstract;
using MyShows.Model.Entities;

namespace MyShows.Data.Repositories
{
    public class CommentRepository : EntityBaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository(MyShowsContext context) : base(context) { }
    }
}