﻿using MyShows.Data.Abstract;
using MyShows.Model.Entities;

namespace MyShows.Data.Repositories
{
    public class PostTagsRepository : EntityBaseRepository<PostTags>, IPostTagsRepository
    {
        public PostTagsRepository(MyShowsContext context) : base(context) { }
    }
}