﻿using MyShows.Data.Abstract;
using MyShows.Model.Entities;

namespace MyShows.Data.Repositories
{
    public class PostRepository : EntityBaseRepository<Post>, IPostRepository
    {
        public PostRepository (MyShowsContext context) : base (context) { }
        
    }
}